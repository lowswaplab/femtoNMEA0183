
// femtoNMEA0183
// https://gitlab.com/lowswaplab/femtoNMEA0183
// Copyright 2017-2018 Low SWaP Lab lowswaplab.com

#include "femtoNMEA0183.h"
#include <string.h>
#include <stdlib.h>

  static uint8_t fromHex(const char c)
    {
    if (c >= 'A' && c <= 'F')
      return(c-'A'+10);
    if (c >= 'a' && c <= 'f')
      return(c-'a'+10);
    return(c-'0');
    }

bool femtoNMEA0183::NMEAchecksum(const char *str, const uint16_t len)
  {
  uint16_t index;
  uint16_t sum;
  uint16_t _sum;

  if (str[0] != '$')
    return(false);

  sum = 0;
  for (index=1; index < len && str[index] != '*'; ++index)
    {
    sum = sum ^ str[index];
    };

  if (str[index] == '*' && len-index == 5)
    {
    _sum = fromHex(str[index+1])*16 + fromHex(str[index+2]);

    if (sum == _sum)
      return(true);
    };

  return(false);
  }

femtoNMEA0183::GPS::GPS() :
  lineIndex(0),
  lineFound(false)
  {
  bzero(line, 128);
  }

bool femtoNMEA0183::GPS::read(const char letter)
  {
  if (lineFound || lineIndex >= 128)
    {
    bzero(line, 128);
    lineIndex = 0;
    lineFound = false;
    };

  line[lineIndex] = letter;
  ++lineIndex;

  if (letter == '\n' || letter == '\r')
    {
    line[lineIndex] = '\0';
    ++lineIndex; // so it equals line length, not just index
    lineFound = true;

    return(true);
    };

  return(false);
  }

femtoNMEA0183::GPRMC::GPRMC()
  {
  reset();
  }

void femtoNMEA0183::GPRMC::reset()
  {
  hour = 0;
  minute = 0;
  second = 0;
  valid = false;
  latitude = 0;
  longitude = 0;
  year = 0;
  month = 0;
  day = 0;
  sog = 0;
  cmg = 0;
  magvar = 0;
  }

void femtoNMEA0183::GPRMC::operator=(const GPRMC &gprmc)
  {
  hour = gprmc.hour;
  minute = gprmc.minute;
  second = gprmc.minute;
  valid = gprmc.valid;
  latitude = gprmc.latitude;
  longitude = gprmc.longitude;
  year = gprmc.year;
  month = gprmc.month;
  day = gprmc.day;
  sog = gprmc.sog;
  cmg = gprmc.cmg;
  magvar = gprmc.magvar;
  }

bool femtoNMEA0183::GPRMC::parse(const char *line, const int8_t len,
                                const bool _reset)
  {
  if (strncmp(line, "$GPRMC,", 7) != 0)
    return(false);

  if (NMEAchecksum(line, len) == false)
    return(false);

  uint16_t index;
  uint16_t fCount=0;
  uint16_t fstart=0;

  if (_reset == true)
    reset();

  for (index=0; index < len; ++index)
    {
    if (line[index] == ',' || line[index] == '*')
      {
      ++fCount;
      if (fCount == 2 && index-fstart >= 6)    // HHMMSS
        {
        hour = ((line[fstart]-48)*10) + (line[fstart+1]-48);
        minute = ((line[fstart+2]-48)*10) + (line[fstart+3]-48);
        second = ((line[fstart+4]-48)*10) + (line[fstart+5]-48);
        }
      else if (fCount == 3 && index-fstart == 1) // valid
        {
        if (line[fstart] == 'A')
          valid = true;
        }
      else if (fCount == 4 && index-fstart >= 5) // latitude
        {
        uint8_t latdeg;
        float latmin;

        latdeg = ((line[fstart]-48)*10) + (line[fstart+1]-48);
        latmin = atof(line+fstart+2);

        latitude = latdeg + (latmin / 60.0);
        }
      else if (fCount == 5 && index-fstart == 1) // latitude hemisphere
        {
        if (line[fstart] == 'S')
          latitude *= -1;
        }
      else if (fCount == 6 && index-fstart >= 6) // longitude
        {
        uint8_t londeg;
        float lonmin;

        londeg = ((line[fstart]-48)*100) + ((line[fstart+1]-48)*10) +
                 (line[fstart+2]-48);
        lonmin = atof(line+fstart+3);

        longitude = londeg + (lonmin / 60.0);
        }
      else if (fCount == 7 && index-fstart == 1) // longitude hemisphere
        {
        if (line[fstart] == 'W')
          longitude *= -1;
        }
      else if (fCount == 8 && index-fstart >= 2) // speed over ground
        {
        sog = atof(line+fstart);
        }
      else if (fCount == 9 && index-fstart >= 2) // course made good
        {
        cmg = atof(line+fstart);
        }
      else if (fCount == 10 && index-fstart == 6) // date
        {
        day = ((line[fstart]-48)*10) + (line[fstart+1]-48);
        month = ((line[fstart+2]-48)*10) + (line[fstart+3]-48);
        year = 2000 + ((line[fstart+4]-48)*10) + (line[fstart+5]-48);
        }
      else if (fCount == 11 && index-fstart >= 2) // mag variation
        {
        magvar = atof(line+fstart);
        }
      else if (fCount == 12 && index-fstart == 1) // mag var direction
        {
        if (line[fstart] == 'W')
          magvar *= -1;
        };

      fstart = index + 1;
      };
    };

  return(true);
  }

femtoNMEA0183::GPGGA::GPGGA()
  {
  reset();
  }

void femtoNMEA0183::GPGGA::reset()
  {
  hour = 0;
  minute = 0;
  second = 0;
  latitude = 0;
  longitude = 0;
  quality = 0;
  satellites = 0;
  hdop = 0;
  altitude = 0;
  hae = 0;
  }

bool femtoNMEA0183::GPGGA::parse(const char *line, const int8_t len,
                                const bool _reset)
  {
  if (strncmp(line, "$GPGGA,", 7) != 0)
    return(false);

  if (NMEAchecksum(line, len) == false)
    return(false);

  uint16_t index;
  uint16_t fCount=0;
  uint16_t fstart=0;

  if (_reset == true)
    reset();

  for (index=0; index < len; ++index)
    {
    if (line[index] == ',' || line[index] == '*')
      {
      ++fCount;
      if (fCount == 3 && index-fstart >= 2)         // latitude
        {
        latitude = (int)(atof(line+fstart) / 100);
        latitude += (atof(line+fstart) - latitude*100) / 60;
        }
      else if (fCount == 4 && index-fstart == 1)    // latitude hemisphere
        {
        if (line[fstart] == 'S')
          latitude *= -1;
        }
      else if (fCount == 5 && index-fstart >= 2)    // longitude
        {
        longitude = (int)(atof(line+fstart)/ 100);
        longitude += (atof(line+fstart) - longitude*100) / 60;
        }
      else if (fCount == 6 && index-fstart == 1)    // longitude hemisphere
        {
        if (line[fstart] == 'W')
          longitude *= -1;
        }
      else if (fCount == 7 && index-fstart == 1)    // quality
        {
        quality = atoi(line+fstart);
        }
      else if (fCount == 8 && index-fstart >= 1)    // satellites
        {
        satellites = atoi(line+fstart);
        }
      else if (fCount == 9 && index-fstart >= 2)    // HDOP
        {
        hdop = atof(line+fstart);
        }
      else if (fCount == 10 && index-fstart >= 2)    // altitude
        {
        altitude = atof(line+fstart);
        }
      else if (fCount == 11 && index-fstart >= 2)    // height above ellipsoid
        {
        hae = atof(line+fstart);
        };

      fstart = index + 1;
      };
    };

  return(true);
  }

femtoNMEA0183::GPGSA::GPGSA()
  {
  reset();
  }

void femtoNMEA0183::GPGSA::reset()
  {
  fixType = 0;
  pdop = 0;
  hdop = 0;
  vdop = 0;
  }

bool femtoNMEA0183::GPGSA::parse(const char *line, const int8_t len,
                                const bool _reset)
  {
  if (strncmp(line, "$GPGSA,", 7) != 0)
    return(false);

  if (NMEAchecksum(line, len) == false)
    return(false);

  uint16_t index;
  uint16_t fCount=0;
  uint16_t fstart=0;

  if (_reset == true)
    reset();

  for (index=0; index < len; ++index)
    {
    if (line[index] == ',' || line[index] == '*')
      {
      ++fCount;
      if (fCount == 3 && index-fstart == 1)          // Fix Type
        {
        fixType = atoi(line+fstart);
        }
      else if (fCount == 16 && index-fstart >= 2)    // PDOP
        {
        pdop = atof(line+fstart);
        }
      else if (fCount == 17 && index-fstart >= 2)    // HDOP
        {
        hdop = atof(line+fstart);
        }
      else if (fCount == 18 && index-fstart >= 2)    // HDOP
        {
        vdop = atof(line+fstart);
        };

      fstart = index + 1;
      };
    };

  return(true);
  }

femtoNMEA0183::GPVTG::GPVTG()
  {
  reset();
  }

void femtoNMEA0183::GPVTG::reset()
  {
  tmg = -1;
  tmgm = -1;
  sogkn = -1;
  sog = -1;
  }

bool femtoNMEA0183::GPVTG::parse(const char *line, const int8_t len,
                                const bool _reset)
  {
  if (strncmp(line, "$GPVTG,", 7) != 0)
    return(false);

  if (NMEAchecksum(line, len) == false)
    return(false);

  uint16_t index;
  uint16_t fCount=0;
  uint16_t fstart=0;

  if (_reset == true)
    reset();

  for (index=0; index < len; ++index)
    {
    if (line[index] == ',' || line[index] == '*')
      {
      ++fCount;
      if (fCount == 2 && index-fstart >= 2)    // TMG
        {
        tmg = atof(line+fstart);
        }
      else if (fCount == 4 && index-fstart >= 2)    // TMGm
        {
        tmgm = atof(line+fstart);
        }
      else if (fCount == 6 && index-fstart >= 2)    // SOGkn
        {
        sogkn = atof(line+fstart);
        }
      else if (fCount == 8 && index-fstart >= 2)    // SOG
        {
        sog = atof(line+fstart);
        };

      fstart = index + 1;
      };
    };

  return(true);
  }

