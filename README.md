
# femtoNMEA0183

femtoNMEA0183 is an uber-tiny NMEA0183 parser suitable for
microcontrollers such as ATmega328 (e.g. used on Arduino boards).

# Resources

## Source code

Source code can be found on GitLab:
[https://gitlab.com/lowswaplab/femtoNMEA0183](https://gitlab.com/lowswaplab/femtoNMEA0183)

## Issue tracker

Issues can e submitted and tracked on GitLab:
[https://gitlab.com/lowswaplab/femtoNMEA0183/issues](https://gitlab.com/lowswaplab/femtoNMEA0183/issues)

# Copyright

Copyright 2017-2018 Low SWaP Lab [lowswaplab.com](https://lowswaplab.com/)

