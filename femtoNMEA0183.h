
// femtoNMEA0183
// https://gitlab.com/lowswaplab/femtoNMEA0183
// Copyright 2017-2018 Low SWaP Lab lowswaplab.com

#ifndef femtoNMEA0183_H
  #define femtoNMEA0183_H

  #include <inttypes.h>

  namespace femtoNMEA0183
    {
    class GPS
      {
      public:
        GPS();
        bool read(const char letter);

        char line[128];
        unsigned short lineIndex;
        bool lineFound;
      };

    bool NMEAchecksum(const char *str, const uint16_t len);

    class GPRMC
      {
      public:
        GPRMC();
        void reset();
        void operator=(const GPRMC &gprmc);
        bool parse(const char *line, const int8_t len,
                  const bool reset=false);

        uint8_t hour;
        uint8_t minute;
        uint8_t second;
        bool valid;
        float latitude;
        float longitude;
        uint16_t year;
        uint8_t month;
        uint8_t day;
        float sog;           // speed over ground (knots)
        float cmg;           // course made good (true north)
        float magvar;        // magnetic variation (- west, + east)
      };

    class GPGGA
      {
      public:
        GPGGA();
        void reset();
        bool parse(const char *line, const int8_t len,
                   const bool reset=false);

        uint8_t hour;        // XXX not implemented
        uint8_t minute;      // XXX not implemented
        uint8_t second;      // XXX not implemented
        float latitude;
        float longitude;
        uint8_t quality;
        uint8_t satellites;
        float hdop;
        float altitude;
        float hae;           // height above ellipsoid
      };

    class GPGSA
      {
      public:
        GPGSA();
        void reset();
        bool parse(const char *line, const int8_t len,
                   const bool reset=false);

        uint8_t fixType;
        float pdop;
        float hdop;
        float vdop;
      };

    class GPVTG
      {
      public:
        GPVTG();
        void reset();
        bool parse(const char *line, const int8_t len,
                   const bool reset=false);

        float tmg;    // track made good (degrees true)
        float tmgm;   // track made good (degrees magnetic)
        float sogkn;  // speed over ground (knots)
        float sog;    // speed over ground (km/h)
      };
    }

#endif

